<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Formation</title>
    <?php include_once 'inc/head.php'; ?>
</head>

<body>
    <?php include_once 'inc/header.php'; ?>

    <main>
        <article>
            <header>
                <h1>Welcome!</h1>
            </header>
            <p>On étudie l'ingénerie du WEB</p>
            <p>C'est fun!</p>
        </article>
    </main>

    <?php include_once 'inc/footer.php'; ?>
</body>

</html>